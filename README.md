#ReadMe

#Instructions to Run This Code
Run Xampp/wampp or any server in your local machine

Step1: Pull the code from provided URL
Step2: As MySQL database is used here, import clickview.sql file in phpmyadmin or by using any mysql interaction software
Step3: Try hitting url "localhost/clickview_practical" on web browser
Step4: If step3 shows some error then open application > config > database.php file 
	And from line number 78-81, use your configurations
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'clickview',
Step5: Try step3 and it should be working now, if it do not work please do not hesitate to contact me.