<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Register_model', 'register');
    }

    public function index()
    {
    	$data['all_list'] = $this->register->getAllDatas();
    	$this->load->view('landing',$data);
    }
}