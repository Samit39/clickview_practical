<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Register_model', 'register');
    }

	public function index()
	{
		$data['addCss'] = array('assets/css/font-awesome.css');
		$data['addJs'] = array('assets/js/jquery-1.11.1.min.js');
		$this->load->view('register',$data);
	}

	public function create()
	{
		$post = $_POST;
		if(!empty($post))
		{
			$this->form_validation->set_rules($this->register->rules());
			if ($this->form_validation->run() == true)
			{
				$post['created'] = date("Y-m-d h:i:s");
				$inserted_id = $this->register->insert_new($post);
			}
		}
		redirect('landing');
	}
}	