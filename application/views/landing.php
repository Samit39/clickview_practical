<!--
Author: Samit Puri
-->
<!DOCTYPE HTML>
<html>
	<head>
		<title>Click View Practical Landing Page</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Adding Custom Css -->
		<?php if(isset($addCss) && !empty($addCss)) {
		foreach($addCss as $css) { ?>
		<link href="<?php ?>" rel="stylesheet" type="text/css" />
		<?php }
		} ?>
		<!-- Adding Custom Css ends-->
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/w/dt/dt-1.10.18/datatables.min.css"/>
	</head>
	<body>
		<div class="container landing">
			<div class="row">
				<div class="col-md-12 pb-1 heading-display">
					<h2>Click View Customer Information Collection Practical</h2>
				</div>
				<div class="col-md-12 pb-1 top-left-button">
					<a href="<?php echo base_url().'register' ?>" class="btn btn-rounded btn-warning"><i class="fa fa-plus" aria-hidden="true"></i> Add Information</a>
				</div>
				<div class="col-md-12 table-display">
					<div class="table-responsive">
						<table class="table table-hover" id="contact_table">
							<thead>
								<tr>
									<th >SN</th>
									<th >First Name</th>
									<th >Last Name</th>
									<th >Email</th>
									<th >Phone Number</th>
									<th >School</th>
									<th >Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(isset($all_list) && !empty($all_list))
								{
								$i = 1;
								foreach ($all_list as $value) {
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $value->fname; ?></td>
									<td><?php echo $value->lname; ?></td>
									<td><?php echo $value->email; ?></td>
									<td><?php echo $value->phone; ?></td>
									<td><?php echo $value->school; ?></td>
									<td>
										<a href="javascript:void(0)" class="btn btn-sm btn-primary table-action-icon" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
										<a href="javascript:void(0)" class="btn btn-sm btn-danger table-action-icon" title="delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</td>

								</tr>
								<?php
								$i++;
								} //foreach ends here
								}//isset ends here
								else
								{
								?>
								<tr>
									<td colspan="7"><center>No Data Available</center></td>
								</tr>
								<?php
								}//isset else ends here
								?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/w/dt/dt-1.10.18/datatables.min.js"></script>
</html>