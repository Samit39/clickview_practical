<!--
Author: Samit Puri
-->
<!DOCTYPE HTML>
<html>
	<head>
		<title>Click View Practical Register Form</title>
		
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Adding Custom Css -->
		<?php if(isset($addCss) && !empty($addCss)) {
		foreach($addCss as $css) { ?>
		<link href="" rel="stylesheet" type="text/css" />
		<?php }
		} ?>
		<!-- Adding Custom Css ends-->
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
	<body>
		<?php $form_submit_url = base_url().'register/create'; ?>
		<form id="register-formsubmit" autocomplete="off" action="<?php echo $form_submit_url; ?>" method="POST">
			<div class="container contact">
				<div class="row">
					<div class="col-md-12 pb-1 heading-display">
						<h2>Click View Customer Information Collection Practical</h2>
					</div>
					<div class="col-md-12 pb-1 top-left-button">
						<a href="<?php echo base_url().'landing' ?>" class="btn btn-rounded btn-warning"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> View Listing</a>
					</div>
					<div class="col-md-3">
						<div class="contact-info">
							<img src="<?php echo base_url()."assets/images/contact-image.png" ?>" alt="image"/>
							<h2>Contact Us</h2>
							<h4>We promise, we will be in touch with you!</h4>
						</div>
					</div>
					<div class="col-md-9">
						<div class="contact-form">
							<div class="form-group">
								<label class="control-label col-sm-2" for="fname">First Name:</label>
								<div class="col-sm-12">
									<input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="lname">Last Name:</label>
								<div class="col-sm-12">
									<input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="email">Email:</label>
								<div class="col-sm-12">
									<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4" for="phone">Phone Number:</label>
								<div class="col-sm-12">
									<input type="text" class="form-control" id="phone" placeholder="Enter Phone Number" name="phone" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4" for="school">School Name:</label>
								<div class="col-sm-12">
									<input type="text" class="form-control" id="school" placeholder="Enter School Name" name="school" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Submit</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>