<?php

class Register_model extends CI_Model {

    public $table = 'tbl_contacts';

    public function __construct() {
        parent::__construct();
      
    }

    public function rules($id = '') {
        $array = array(
            array(
                'field' => 'fname',
                'label' => 'fname',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'lname',
                'label' => 'lname',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required',
            ), 
            array(
                'field' => 'phone',
                'label' => 'phone',
                'rules' => 'trim|required',
            ), 
            array(
                'field' => 'school',
                'label' => 'school',
                'rules' => 'trim|required',
            ), 
 
        );

        return $array;
    }

    public function insert_new($data)
    {
    	$this->db->insert('tbl_contacts', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
    }

    public function getAllDatas()
    {
    	$this->db->select('t1.*');
        $this->db->from('tbl_contacts t1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
        	return $query->result();
        }
        else
        {
        	return false;
        }
    }
}